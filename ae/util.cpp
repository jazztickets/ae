/******************************************************************************
* Copyright (c) 2025 Alan Witkowski
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*******************************************************************************/
#include <ae/util.h>
#include <sys/stat.h>
#include <SDL_timer.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <regex>

namespace ae {

static uint64_t Timer = 0;

// Loads a file into a string
const char *LoadFileIntoMemory(const char *Path) {

	// Open file
	std::ifstream File(Path, std::ios::binary);
	if(!File)
		return nullptr;

	// Get file size
	File.seekg(0, std::ios::end);
	std::ifstream::pos_type Size = File.tellg();
	if(!Size)
		return nullptr;

	File.seekg(0, std::ios::beg);

	// Read data
	char *Data = new char[(size_t)Size + 1];
	File.read(Data, Size);
	File.close();
	Data[(size_t)Size] = 0;

	return Data;
}

// Remove extension from a filename
std::string RemoveExtension(const std::string &Path) {
	size_t SuffixPosition = Path.find_last_of(".");
	if(SuffixPosition == std::string::npos)
		return Path;

	return Path.substr(0, SuffixPosition);
}

// Trim whitespace from string
std::string TrimString(const std::string &String) {
	std::regex Regex("^[\\s]+|[\\s]+$");
	return std::regex_replace(String, Regex, "");
}

// Create directory
int MakeDirectory(const std::string &Path) {
#ifdef _WIN32
	return mkdir(Path.c_str());
#else
	return mkdir(Path.c_str(), 0755);
#endif
}

// Tokenize a string by a delimiting char
void TokenizeString(const std::string &String, std::vector<std::string> &Tokens, char Delimiter) {
	std::stringstream Buffer(String);
	std::string Token = "";
	while(std::getline(Buffer, Token, Delimiter))
		Tokens.push_back(std::move(Token));
}

// Start timer
void StartTimer() {
	Timer = SDL_GetPerformanceCounter();
}

// Print timer
void PrintTimer(const std::string &Message, bool Reset) {
	double Time = (SDL_GetPerformanceCounter() - Timer) / (double)SDL_GetPerformanceFrequency();
	if(!Message.empty())
		std::cout << Message << ": ";

	std::cout << std::fixed << std::setprecision(5) << Time << std::endl;
	if(Reset)
		StartTimer();
}

// Format numbers larger than 9999 with SI suffix
template<typename T> void FormatSI(std::stringstream &Buffer, T Number, RoundFunction *RoundFunction) {
	if(std::abs(Number) >= 1e18)
		Buffer << RoundFunction(Number / 1e18) << "E";
	else if(std::abs(Number) >= 1e15)
		Buffer << RoundFunction(Number / 1e15) << "P";
	else if(std::abs(Number) >= 1e12)
		Buffer << RoundFunction(Number / 1e12) << "T";
	else if(std::abs(Number) >= 1e9)
		Buffer << RoundFunction(Number / 1e9) << "G";
	else if(std::abs(Number) >= 1e6)
		Buffer << RoundFunction(Number / 1e6) << "M";
	else if(std::abs(Number) >= 1e4)
		Buffer << RoundFunction(Number / 1e3) << "K";
	else
		Buffer << Number;
}


// Format time as 00:00:00.00
void FormatTime(std::stringstream &Buffer, double Time) {
	uint32_t Hours = Time / 3600;
	uint32_t Minutes = (uint32_t)(Time / 60) % 60;
	uint32_t Seconds = (uint32_t)(Time) % 60;
	uint32_t Centiseconds = (uint32_t)((Time - (uint32_t)(Time)) * 100);
	Buffer
		<< std::setw(2) << std::setfill('0') << Hours << ':'
		<< std::setw(2) << std::setfill('0') << Minutes << ':'
		<< std::setw(2) << std::setfill('0') << Seconds << '.'
		<< std::setw(2) << std::setfill('0') << Centiseconds;
}

// Format time as hours/minutes/seconds
void FormatTimeHMS(std::stringstream &Buffer, int64_t Time) {
	if(Time < 60)
		Buffer << Time << "s";
	else if(Time < 3600)
		Buffer << Time / 60 << "m";
	else
		Buffer << Time / 3600 << "h" << (Time / 60 % 60) << "m";
}

template void FormatSI<int64_t>(std::stringstream &Buffer, int64_t Number, RoundFunction *RoundFunction);
template void FormatSI<double>(std::stringstream &Buffer, double Number, RoundFunction *RoundFunction);

}
