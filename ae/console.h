/******************************************************************************
* Copyright (c) 2025 Alan Witkowski
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*******************************************************************************/
#pragma once

// Libraries
#include <glm/vec4.hpp>
#include <fstream>
#include <string>
#include <vector>

namespace ae {

// Forward Declarations
class _Element;
class _Font;
class _Program;
struct _Style;

// Console class
class _Console {

	// Console message
	struct _Message {
		std::string Text;
		glm::vec4 Color;
	};

	public:

		_Console(const _Program *Program, const _Font *Font);
		~_Console();

		void LoadHistory(const std::string &Path);
		void UpdateSize();
		void Update(double FrameTime);
		void Render(double BlendFactor);

		bool IsOpen();
		void Toggle();
		void AddMessage(const std::string &Text, bool Log=false, const glm::vec4 &Color=glm::vec4(0.5f, 0.5f, 0.5f, 1.0f));
		void UpdateHistory(int Direction);
		void Scroll(int Direction);

		// Commands
		std::vector<std::string> CommandList;
		std::string Command;
		std::string Parameters;

	private:

		// Messages
		std::vector<_Message> Messages;

		// Commands
		std::fstream HistoryFile;
		std::vector<std::string> CommandHistory;
		std::vector<std::string>::iterator CommandHistoryIterator;

		// UI
		_Element *Element{nullptr};
		_Element *TextboxElement{nullptr};
		_Style *Style{nullptr};
		_Style *InputStyle{nullptr};
		const _Font *Font{nullptr};
		int ScrollPosition{0};
};

}
